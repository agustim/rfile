package main

import (
          "github.com/shirou/gopsutil/disk"
          "io/ioutil"
          "math/rand"
          "os"
          "fmt"
          "strconv"
          "time"
          "io"
          "path/filepath"
        )

const defaultSize = "100"

func buildFileName() string {
    return "file-" + time.Now().Format("20060102150405") + "-" + strconv.Itoa(random(100,999)) + ".chunk"
}

func random(min, max int) int {
    return rand.Intn(max - min) + min
}

func createRandomFile(filename string, chuncks int) {
  bigBuff := make([]byte, chuncks * 1024 * 1024)
  rand.Read(bigBuff)
  ioutil.WriteFile( filename , bigBuff, 0666)
}

func copyFileContents(src, dst string) (err error) {
    in, err := os.Open(src)
    if err != nil {
        fmt.Println(err)
        return
    }
    defer in.Close()
    out, err := os.Create(dst)
    if err != nil {
        return
    }
    defer func() {
        cerr := out.Close()
        if err == nil {
            err = cerr
        }
    }()
    if _, err = io.Copy(out, in); err != nil {
        return
    }
    err = out.Sync()
    return
}

func freeChunkSpace(chunkSize int) int {
  var retInt int
  parts, err := disk.Partitions(false)
  if err != nil {
      fmt.Println(err)
  }

  var usage []*disk.UsageStat

  part := parts[0]
  u, err := disk.Usage(part.Mountpoint)
  if err != nil {
      fmt.Println(err)
  }
  usage = append(usage, u)
  retInt = int(u.Free/1024/1024/uint64(chunkSize))
  if err != nil {
      fmt.Println(err)
  }
  return int(retInt)

}
func removeChunks() {
  files, err := filepath.Glob("*.chunk")
  if err != nil {
      panic(err)
  }
  for _, f := range files {
      if err := os.Remove(f); err != nil {
          panic(err)
      }
  }
}

func main() {
  var sizeMB string

  var countChunks int
  var indexChunks int = 0

  if len(os.Args) < 2 {
    fmt.Println("Sense parametres, utilitzem el valor per defecte: " + defaultSize)
    sizeMB = defaultSize
  } else {
    sizeMB = os.Args[1]
  }

  chunckSize,_ := strconv.Atoi(sizeMB)

  if len(os.Args) < 3 {
    fmt.Println("Sense numero de chunks, utilitzem tot el disk per defecte: " + defaultSize)
    countChunks = freeChunkSpace(chunckSize)
  } else {
    countChunks,_ = strconv.Atoi(os.Args[2])
  }


  fmt.Printf("Total %d Chunks de %d MB\n", countChunks, chunckSize)
  rand.Seed(time.Now().Unix())
  fnameOrigin := buildFileName()
  fmt.Println(fnameOrigin + ":" + sizeMB + " - " + strconv.Itoa(indexChunks))
  createRandomFile(fnameOrigin,chunckSize)
  indexChunks++

  for indexChunks <= countChunks {
    fname := buildFileName()
    fmt.Println(fname + ":" + sizeMB + " - " + strconv.Itoa(indexChunks))
    copyFileContents(fnameOrigin,fname)
    indexChunks++
  }
  if len(os.Args) > 3 {
    if os.Args[3] != "no-remove" {
      removeChunks()
    }
  } else {
    removeChunks()
  }

}
